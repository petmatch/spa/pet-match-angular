export const urlsConfig = Object.freeze({
  menu: 'https://petmatch-ms-bff.herokuapp.com/menu',
  login: '/local/login',
  users: 'https://petmatch-ms-bff.herokuapp.com/matches',
  match: 'https://petmatch-ms-match.herokuapp.com/matches',
  matchList: '/local/match-list',
  adotados: '/local/adotados',
  pets: '/local/pets',
  addPet: 'https://petmatch-microservice-animal.herokuapp.com/animal',
  fotosPet: 'https://petmatch-microservice-animal.herokuapp.com/fotos',
  ongPets: 'https://petmatch-microservice-animal.herokuapp.com/ong',
  addPessoa: 'https://petmatch-microservice-pessoa.herokuapp.com/pessoa',
  fotosPessoa: 'https://petmatch-microservice-pessoa.herokuapp.com/fotos',
  user: 'https://petmatch-microservice-pessoa.herokuapp.com/usuario'
})
