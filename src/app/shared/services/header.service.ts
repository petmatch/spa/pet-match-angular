import { Injectable, Injector } from '@angular/core';
import { MenuModel } from '../model/menu.model';
import { urlsConfig } from './../../config/urls.config';
import { CrudService } from './crud.service';

@Injectable({
  providedIn: 'root'
})
export class HeaderService extends CrudService<MenuModel> {

  constructor(
    protected injector: Injector,
  ) {
    super(
      urlsConfig.menu,
      injector,
      MenuModel.fromJson
    );
  }
}
