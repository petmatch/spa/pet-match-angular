import { Injectable, Injector } from '@angular/core';
import { urlsConfig } from './../../config/urls.config';
import { MatchList } from './../model/match-list.interface';
import { CrudService } from './crud.service';

@Injectable({
  providedIn: 'root'
})
export class MatchListService extends CrudService<MatchList> {

  constructor(
    protected injector: Injector,
  ) {
    super(
      urlsConfig.users,
      injector,
      MatchList.fromJson
    );
  }
}
