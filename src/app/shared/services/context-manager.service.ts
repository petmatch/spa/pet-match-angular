import { BehaviorSubject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ContextManagerService {

  public $idUser: number;
  public _idUser: BehaviorSubject<number> = new BehaviorSubject(null);
  public idUser: Observable<number> = this._idUser.asObservable();


  public $idOng: number;
  public _idOng: BehaviorSubject<number> = new BehaviorSubject(null);
  public idOng: Observable<number> = this._idOng.asObservable();

  constructor() {

    this.idUser.subscribe(val => this.$idUser = val);
    this.idOng.subscribe(val => this.$idOng = val);

  }
}
