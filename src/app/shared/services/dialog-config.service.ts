import { DialogData } from './../interfaces/dialog-data.interface';
import { Injectable } from '@angular/core';
import { MatDialogConfig } from '@angular/material/dialog';

@Injectable({
  providedIn: 'root'
})
export class DialogConfigService {

  public dialogConfig: MatDialogConfig;

  constructor() { }

  public modalConfig(dialogData: DialogData): MatDialogConfig {
    this.dialogConfig.disableClose = true;
    this.dialogConfig.data = dialogData;

    return this.dialogConfig;
  }
}
