import { Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

export abstract class CrudService<T> {

  protected httpClient:HttpClient;

  /**
   * Sempre definir o apiPath ao utilizar a Classe Genérica
   * @param apiPath - Caminho para a chamada HTTP
   */
  constructor(
    private apiPath: string,
    protected injector: Injector,
    protected jsonDataToResourceFn: (jsonData: any) => T
  ) {
    this.httpClient = injector.get(HttpClient);
  }

  public create(request: T, extraPath?: string, headers?): Observable<T> {
    const realPath = extraPath ? extraPath : '';
    return this.httpClient
      .post(`${this.apiPath}${realPath}`, request, {headers: headers})
      .pipe(
        map(this.jsonDataToResource.bind(this)),
        catchError(this.handleError)
      );
  }

  public read(extraPath?: string): Observable<T[]> {
    const realPath = extraPath ? extraPath : '';
    return this.httpClient
      .get(`${this.apiPath}${realPath}`)
      .pipe(
        map(this.jsonDataToResources.bind(this)),
        catchError(this.handleError)
      );
  }

  public readOne(extraPath?: string): Observable<T> {
    const realPath = extraPath ? extraPath : '';
    return this.httpClient
      .get(`${this.apiPath}${realPath}`)
      .pipe(
        map(this.jsonDataToResource.bind(this)),
        catchError(this.handleError)
      );
  }

  public getById(id: number,extraPath?: string): Observable<T> {
    const realPath = extraPath ? extraPath : '';
    return this.httpClient
      .get(`${this.apiPath}${realPath}`)
      .pipe(
        map(this.jsonDataToResource.bind(this)),
        catchError(this.handleError)
      );
  }

  public update(request?: T,  extraPath?: string): Observable<T> {
    const realPath = extraPath ? extraPath : '';
    return this.httpClient
      .put(`${this.apiPath}${realPath}`, request)
      .pipe(
        map(this.jsonDataToResource.bind(this)),
        catchError(this.handleError)
      );
  }

  public delete(id: number, extraPath?: string): Observable<T> {
    const realPath = extraPath ? extraPath : '';
    return this.httpClient
      .delete(`${this.apiPath}${realPath}/${id}`)
      .pipe(
        map(this.jsonDataToResource.bind(this)),
        catchError(this.handleError)
      );
  }

  // PROTECTED METHODS

  protected jsonDataToResources(jsonData): T[] {
    const resources: T[] = [];
    jsonData.forEach(
      element => resources.push( element )
    );
    return resources;
  }

  protected jsonDataToResource(jsonData: any): T {
    return this.jsonDataToResourceFn(jsonData);
  }

  protected handleError(error: any): Observable<any>{
    return throwError(() => error);
  }
}
