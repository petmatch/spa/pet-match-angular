import { Adotados } from './../model/adotados.interface';
import { urlsConfig } from './../../config/urls.config';
import { CrudService } from './crud.service';
import { Injectable, Injector } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AdotadosService extends CrudService<Adotados> {

  constructor(
    protected injector: Injector,
  ) {
    super(
      urlsConfig.adotados,
      injector,
      Adotados.fromJson

    );
  }
}
