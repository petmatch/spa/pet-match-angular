import { UserModel } from './../model/user.model';
import { urlsConfig } from './../../config/urls.config';
import { Injectable, Injector } from '@angular/core';
import { CrudService } from './crud.service';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService extends CrudService<UserModel> {

  public $user: UserModel;

  public user: BehaviorSubject<UserModel> = new BehaviorSubject(new UserModel());
  public _user: Observable<UserModel> = this.user.asObservable();

  public $isLoggedIn: boolean;
  public isLoggedIn: BehaviorSubject<boolean> = new BehaviorSubject(false);
  public _isLoggedIn: Observable<boolean> = this.isLoggedIn.asObservable();
  constructor(
    protected injector: Injector,
  ) {
    super(
      urlsConfig.user,
      injector,
      UserModel.fromJson
    );

    this._user.subscribe(val => {
      Object.assign(this.$user = val);
      this.isLoggedIn.next(this.hasValues(this.$user));
    });

    this._isLoggedIn.subscribe(val => this.$isLoggedIn = val);

  }

  private hasValues(obj): boolean {
    return Object.values(obj).some(v => v !== null && typeof v !== "undefined")
  }

}
