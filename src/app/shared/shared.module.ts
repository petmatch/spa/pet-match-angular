
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgxMaskModule } from 'ngx-mask';
import { DialogComponent } from './components/dialog/dialog.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { InputFieldComponent } from './components/input-field/input-field.component';
import { LoaderComponent } from './components/loader/loader.component';
import { LoginComponent } from './components/login/login.component';
import { PetImageComponent } from './components/pet-image/pet-image.component';
import { SidenavComponent } from './components/sidenav/sidenav.component';
import { SlideToggleComponent } from './components/slide-toggle/slide-toggle.component';
import { MaterialModule } from './material/material.module';
import { DetailComponentComponent } from './components/detail-component/detail-component.component';

@NgModule({
  declarations: [
    HeaderComponent,
    SidenavComponent,
    FooterComponent,
    DialogComponent,
    LoginComponent,
    InputFieldComponent,
    SlideToggleComponent,
    PetImageComponent,
    LoaderComponent,
    DetailComponentComponent,
  ],
  imports: [
    HttpClientModule,
    CommonModule,
    MaterialModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule,
    NgxMaskModule.forChild()
  ],
  exports: [
    // shared modules
    CommonModule,
    ReactiveFormsModule,

    RouterModule,
    MaterialModule,
    HttpClientModule,
    // BrowserModule,
    // BrowserAnimationsModule,

    HeaderComponent,
    SidenavComponent,
    FooterComponent,
    DialogComponent,
    LoginComponent,
    InputFieldComponent,
    SlideToggleComponent,
    PetImageComponent,
    LoaderComponent,
    DetailComponentComponent,
  ],
  entryComponents: [DialogComponent]
})
export class SharedModule { }
