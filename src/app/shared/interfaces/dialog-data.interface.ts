export interface DialogData{
  title?: string;
  description?: string;
  signupBtn?: string;
  closeBtn?: CloseBtnDialogData;
  saveBtn?: SaveBtnDialogData;
}

export class SaveBtnDialogData {
  text?: string;
  callback?: any;
}

export class CloseBtnDialogData {
  text?: string;
  color?: string;
}
