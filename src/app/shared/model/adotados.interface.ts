import { ResourceModel } from "./resource.model";

export class Adotados extends ResourceModel{
  constructor(

    public id? : number,
    public petch? : string,
    public sexo? : string,
    public idade? : number,
    public ong? : string,
    public data? : string,
    public imgPetch? : string,
    public description? : string
  ){
    super();
  }

  static fromJson(jsonData: any): Adotados {
    return Object.assign(new Adotados(), jsonData);
  }
}
