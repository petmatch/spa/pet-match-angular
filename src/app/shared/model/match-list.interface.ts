import { ResourceModel } from "./resource.model";

export class MatchList extends ResourceModel{
  constructor(
    public personId?: number,
    public animalId?: number,
    public personName?: string,
    public email?: string,
    public telephone?: string,
    public personImage?: string,
    public animalName?: string,
    public animalImage?: string,
    public animalDescription?: string,
    public type?: string,
    public status?: string,
    public createdAt?: string,
    public updatedAt?: string
  ){
    super();
  }

  static fromJson(jsonData: any): MatchList {
    return Object.assign(new MatchList(), jsonData);
  }
}
