import { ResourceModel } from "./resource.model";

export class AddressModel extends ResourceModel{
  constructor(
    public logradouro? : string,
    public numero? : string,
    public complemento? : string,
    public bairro? : string,
    public cidade? : string,
    public cep? : string,
    public uf? : string,
  ){
    super();
  }

  static fromJson(jsonData: any): AddressModel {
    return Object.assign(new AddressModel(), jsonData);
  }
}
