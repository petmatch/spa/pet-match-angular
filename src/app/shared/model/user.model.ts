import { ResourceModel } from "./resource.model";

export class UserModel extends ResourceModel{
  constructor(
    public personType?: string,
    public login?: string,
    public password?: string,
    public tipo?: string,
    public autenticado?: Boolean
  ){
    super();
  }

  static fromJson(jsonData: any): UserModel {
    return Object.assign(new UserModel(), jsonData);
  }
}
