export class MenuModel {
  constructor(
    public title?: string,
    public route?: string,
    public icon?:string,
  ){

  }

  static fromJson(jsonData: any): MenuModel {
    return Object.assign(new MenuModel(), jsonData);
  }
}
