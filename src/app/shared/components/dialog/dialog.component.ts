import { DialogData } from './../../interfaces/dialog-data.interface';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent{
  public dialog: DialogData;

  constructor(
    public dialogRef: MatDialogRef<DialogComponent>,
    @Inject(MAT_DIALOG_DATA) dialogEl: DialogData

  ) {
    this.dialog = dialogEl;
   }

   public close(): void{
    this.dialogRef.close();
  }

}
