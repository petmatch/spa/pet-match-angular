import { MenuModel } from '../../model/menu.model';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {

  @Input() menuItems: MenuModel[];
  constructor() { }

  ngOnInit(): void {
  }

}
