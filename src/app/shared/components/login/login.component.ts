import { ContextManagerService } from './../../services/context-manager.service';
import { Component, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { LoginService } from '../../services/login.service';
import { ResourceFormComponent } from '../resource-form/resource-form.component';
import { DialogData } from './../../../shared/interfaces/dialog-data.interface';
import { UserModel } from './../../model/user.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent extends ResourceFormComponent<UserModel> {

  public dialogData: DialogData = {
    title: ''
  };

  constructor(

    protected loginService: LoginService,
    protected injector: Injector,
    public dialogRef: MatDialogRef<LoginComponent>,
    protected matDialog: MatDialog,
    private contextManagerService: ContextManagerService
  ) {
    super(injector, new UserModel(), loginService, UserModel.fromJson, matDialog);
    this.pageTitle = 'Faça seu login';
  }

  protected buildResourceForm(): void {
    this.resourceForm = this.formBuilder.group({
      personType: [false],
      email: [null, [Validators.required, Validators.email]],
      password: [null,[Validators.required, Validators.minLength(5)]]
    });
  }

  close(): void {
    this.dialogRef.close();
  }

  signup():void{
    this.close();
    this.router.navigate(['/create-account']);
  }

  login(): void{
/*
    const resource: UserModel = this.jsonDataToResourceFn(this.resourceForm.value);
    resource.personType = this.getFormControls().personType.value === true ? 'PJ' : 'PF';

    this.loginService.readOne().subscribe({
      next: (value) => {
        if(JSON.stringify(value) === JSON.stringify(resource)){
          this.close();

          // adicionar id do usuário aqui
          this.contextManagerService._idUser.next(0);

          this.loginService.user.next(resource);
          this.router.navigate(['/home', resource.personType]);
        }
        else{
          this.errorHandler();
        }
      },
      error: (error) => {
        this.errorHandler();
      }
    })

    */

    let usuario: UserModel = {
     
      login : this.getFormControls().email.value,
      password : this.getFormControls().password.value,      
    
    }; 
   

     this.loginService.create(usuario, '/login').subscribe((
      data => {  
        console.log(data) ; 
        console.log(data.autenticado) ; 
        if (data.autenticado)
        { 
          this.router.navigate(['/home', data.tipo]);
        }
        else{
          alert("usuário inválido!");
        }

      }))
  }

  errorHandler(): void{
    this.dialogData.title = 'Erro ao tentar logar, tente novamente.';
    this.dialogData.saveBtn.text = 'OK';
    this.matDialogConfig = {
      panelClass: 'dialogClass',
      data: this.dialogData
    };
    this.actionsForError();
  }



}
