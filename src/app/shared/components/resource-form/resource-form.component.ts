import { DialogData } from './../../interfaces/dialog-data.interface';
import { DialogComponent } from './../dialog/dialog.component';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { AfterContentChecked, Directive, Injector, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { switchMap } from 'rxjs';
import { ResourceModel } from '../../model/resource.model';
import { CrudService } from './../../services/crud.service';

@Directive()
export abstract class ResourceFormComponent<T extends ResourceModel> implements OnInit, AfterContentChecked {
  public isLoading: boolean = false;
  public currentAction: string;
  public resourceForm: FormGroup;
  public pageTitle: string;
  public serverErrorMessages: string[] = null;
  public submittingForm: boolean = false;
  public matDialogConfig: MatDialogConfig;
  public redirectPath: string

  protected route: ActivatedRoute;
  protected router: Router;
  protected formBuilder: FormBuilder;

  constructor(
    protected injector: Injector,
    public resource: T,
    protected crudService: CrudService<T>,
    protected jsonDataToResourceFn: (jsonData) => T,
    protected dialog?: MatDialog

  ) {
    this.route = this.injector.get(ActivatedRoute);
    this.router = this.injector.get(Router);
    this.formBuilder = this.injector.get(FormBuilder);
  }

  ngOnInit() {
    this.setCurrentAction();
    this.buildResourceForm();
    this.loadResource();
  }

  ngAfterContentChecked(){
    this.setPageTitle();
  }

  submitForm(){
    this.submittingForm = true;

    if(this.currentAction == 'new')
      this.createResource();
    else // currentAction == 'edit'
      this.updateResource();
  }

  getFormControls(){
    return this.resourceForm.controls;
  }

  // PRIVATE METHODS

  protected setCurrentAction() {

    if(this.route.snapshot.url[0] !== undefined) {
      if(this.route.snapshot.url[0].path === 'edit')
        this.currentAction = 'edit'
    }
    else
      this.currentAction = 'new'
  }

  protected loadResource() {
    if (this.currentAction == 'edit') {

      this.route.paramMap.pipe(
        switchMap(params => this.crudService.getById(+params.get('id')))
      )
      .subscribe({
        next: (resource) => {
          this.resource = resource;
          this.resourceForm.patchValue(resource) // binds loaded resource data to resourceForm
        },
        error: (error) => alert('Ocorreu um erro no servidor, tente mais tarde.')
      })
    }
  }


  protected setPageTitle() {
    this.pageTitle = this.getPageTitle;
  }


  protected createResource(){
    this.isLoading = true;
    const resource: T = this.jsonDataToResourceFn(this.resourceForm.value);

    this.crudService.create(resource).subscribe({
      next: (resource) => {this.actionsForSuccess()},
      error: (error) => this.actionsForError()
    });
  }


  protected updateResource(){
    this.isLoading = true;
    const resource: T = this.jsonDataToResourceFn(this.resourceForm.value);

    this.crudService.update(resource)
    .subscribe({
      next: (resource) => {this.actionsForSuccess()},
      error: (error) => this.actionsForError()
      })
  }


  protected actionsForSuccess(){
    this.isLoading = false;
    this.openDialog();

    this.dialog.afterAllClosed.subscribe(() => this.router.navigateByUrl(this.redirectPath));
  }


  protected actionsForError(){

    this.isLoading = false;
    this.submittingForm = false;

    this.openDialog();
    this.dialog.afterAllClosed.subscribe(() => this.router.navigateByUrl(this.redirectPath));

  }

  protected openDialog(): void {
    this.dialog.open(DialogComponent, this.matDialogConfig);
  }


  protected abstract buildResourceForm(): void;

  get getPageTitle(): string{
    return this.pageTitle;
  }
}
