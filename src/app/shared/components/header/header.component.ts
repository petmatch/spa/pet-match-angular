import { Router } from '@angular/router';
import { LoginService } from './../../services/login.service';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSidenav } from '@angular/material/sidenav';
import { MenuModel } from '../../model/menu.model';
import { LoginComponent } from './../login/login.component';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  @Output() sidenavEventEmmit: EventEmitter<MatSidenav> = new EventEmitter();
  @Input() menuItems: MenuModel[];

  constructor(
    public dialog: MatDialog,
    public loginService: LoginService,
    private route:Router) {
    }

  openDialog() {
    if(this.loginService.$isLoggedIn) {
      this.loginService.isLoggedIn.next(false);
      this.route.navigate(['/home']);
    }
    else{
      this.dialog.open(LoginComponent);
    }
  }

  sidenavToggle(evt: any){
    this.sidenavEventEmmit.emit(evt);
  }

}
