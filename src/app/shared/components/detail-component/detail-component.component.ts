import { formatDate } from '@angular/common';
import { Component, Input, OnInit, AfterViewInit } from '@angular/core';
import { ListPetResponse } from './../../../modules/ong/modules/list-pets/model/list-pet-response.model';

@Component({
  selector: 'app-detail-component',
  templateUrl: './detail-component.component.html',
  styleUrls: ['./detail-component.component.scss']
})
export class DetailComponentComponent implements OnInit {
  @Input() animal: ListPetResponse;

  public dtNascimento: string;
  public dtResgate: string;

  constructor() {}

  ngOnInit(): void {
    console.log(this.animal.dtNascimento);
    // this.dtNascimento = formatDate(this.animal.dtNascimento, 'DD/MM/YYYY', 'en');
    // this.dtResgate = formatDate(this.animal.dtResgate, 'DD/MM/YYYY', 'en');
  }

}
