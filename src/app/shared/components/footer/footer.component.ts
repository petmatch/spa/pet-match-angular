import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MenuModel } from '../../model/menu.model';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FooterComponent implements OnInit {
  public footerMenu: MenuModel[] = [
    {
      "title": "Siga-nos no Twitter",
      "route": "https://twitter.com/petmatch",
      "icon": "assets/icons/twitter-brands.svg"
    },
    {
      "title": "Siga-nos no Facebook",
      "route": "https://facebook.com/petmatch",
      "icon": "assets/icons/facebook-f-brands.svg"
    },
    {
      "title": "Assista no Youtube",
      "route": "https://youtube.com/petmatch",
      "icon": "assets/icons/youtube-brands.svg"
    }
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
