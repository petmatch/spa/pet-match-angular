import { FormControl } from '@angular/forms';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-slide-toggle',
  templateUrl: './slide-toggle.component.html',
  styleUrls: ['./slide-toggle.component.scss']
})
export class SlideToggleComponent {

  @Input() checkedControl: FormControl;
  public checked: boolean = false;
  public color: string = 'primary';
  @Input() firstValue: string;
  @Input() lastValue: string;

  constructor() { }

  changed(){
    this.checked = this.checkedControl.value;
  }
}
