/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { PetImageComponent } from './pet-image.component';

describe('PetImageComponent', () => {
  let component: PetImageComponent;
  let fixture: ComponentFixture<PetImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PetImageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PetImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
