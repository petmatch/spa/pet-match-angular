import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-pet-image',
  templateUrl: './pet-image.component.html',
  styleUrls: ['./pet-image.component.scss']
})
export class PetImageComponent implements OnInit {

  @Input() petName: string;
  @Input() petImage: string;

  constructor() { }

  ngOnInit() {
  }

}
