import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-input-field',
  templateUrl: './input-field.component.html',
  styleUrls: ['./input-field.component.scss']
})
export class InputFieldComponent {

  @Input() field: FormControl;
  @Input() fieldName: string;
  @Input() mask: string;

  constructor() { }

  getErrorMessage() {
    if (this.field.hasError('required')) {
      return 'Este campo é obrigatório';
    }
    if (this.field.hasError('minlength')) {
      return 'Digite no mínimo até 5 caracteres.';
    }

    return this.field.hasError('email') ? 'E-mail inválido' : '';
  }

  getFieldType(fieldName: string): string{
    switch (fieldName) {
      case 'Senha':
        return 'password';
      case 'Telefone':
        return 'tel';
      default:
        return 'text';
    }
  }

  getMaskType(mask: string): string{
    switch (mask) {
      case 'CPF':
        return '000.000.000-00';
      case 'CNPJ':
        return '00.000.000/0000-00';
      case 'CEP':
        return '00000-000';
      case 'Telefone':
        return '(00) 0000-0000 || (00) 00000-0000 ';
      default:
        break;
    }
  }

}
