import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-pessoa-image',
  templateUrl: './pessoa-image.component.html',
  styleUrls: ['./pessoa-image.component.scss']
})
export class PessoaImageComponent implements OnInit {

  @Input() pessoa: {
    nome?: string;
    image: string;
  };

  constructor() { }

  ngOnInit() {
  }

}
