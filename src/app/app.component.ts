import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MenuModel } from './shared/model/menu.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.Emulated
})
export class AppComponent implements OnInit{

  public menus: MenuModel[] = [
    {
      "title": "Home",
      "route": "/home"
    },
    {
      "title": "Adoções",
      "route": "/adoptions"
    },
    {
      "title": "Apadrinhamento",
      "route": "/sponsorship"
    },
    {
      "title": "ONGs",
      "route": "/ongs"
    },
    {
      "title": "Doações",
      "route": "/donations"
    },
    {
      "title": "Quem somos",
      "route": "/who-we-are"
    }
  ];

  constructor() { }

  ngOnInit(){
  }
}
