import { Injectable, Injector } from '@angular/core';
import { CrudService } from './../../../shared/services/crud.service';
import { urlsConfig } from './../../../config/urls.config';
import { FotoPessoaModel } from './../model/foto-pessoa.model';

@Injectable({
  providedIn: 'root'
})
export class FotosPessoaService extends CrudService<any> {

  constructor(protected injector: Injector) {
    super(
      urlsConfig.fotosPessoa,
      injector,
      FotoPessoaModel.fromJson
    )
  }

}
