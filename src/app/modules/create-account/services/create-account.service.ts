import { CreateAccountModel } from './../model/create-account.model';
import { urlsConfig } from './../../../config/urls.config';
import { Injectable, Injector } from '@angular/core';
import { CrudService } from 'src/app/shared/services/crud.service';

@Injectable({
  providedIn: 'root'
})
export class CreateAccountService extends CrudService<CreateAccountModel> {

  constructor(protected injector: Injector) {
    super(
      urlsConfig.addPessoa,
      injector,
      CreateAccountModel.fromJson
    )
  }
}
