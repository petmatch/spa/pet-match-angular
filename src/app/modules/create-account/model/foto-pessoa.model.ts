import { ResourceModel } from './../../../shared/model/resource.model';

export class FotoPessoaModel extends ResourceModel{

  constructor(
    public file?: File,
  ){
    super();
  }


  static fromJson(jsonData: any): FotoPessoaModel[] {
    return Object.assign(new FotoPessoaModel(), jsonData);
  }
}
