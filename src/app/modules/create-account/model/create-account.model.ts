import { UserModel } from '../../../shared/model/user.model';
//import { AddressModel } from './../../../shared/model/address.model';
import { ResourceModel } from "./../../../shared/model/resource.model";


export class CreateAccountModel extends ResourceModel{
  constructor(
   
   // public endereco?: AddressModel,
   // public usuario?: UserModel,
    
    public photo?: string,

   // public id?: number,
    public usuarioId?: string,
    public nome? : string,
    public cpfCnpj? : string,
    public dtNascimento? : string,
    public telefone? : string,
    public email? : string,
    public tipo? : string,
    public foto? : string,
    public endereco?: {

       logradouro? : string,
       numero? : string,
       complemento? : string,
       bairro? : string,
       cidade? : string,
       cep? : string,
       uf? : string,
    },
  
    public fotos? : 
      {
        id? : number,
        isprofilePicture? : boolean
      }
    
  ){
    super();
  }

  static fromJson(jsonData: any): CreateAccountModel {
    return Object.assign(new CreateAccountModel(), jsonData);
  }
}
