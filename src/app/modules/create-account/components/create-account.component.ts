import { Component, Injector, ViewEncapsulation } from '@angular/core';
import { Validators } from '@angular/forms';
import { ResourceFormComponent } from 'src/app/shared/components/resource-form/resource-form.component';
import { CreateAccountModel } from './../model/create-account.model';
import { UserModel } from '../../../shared/model/user.model';
import { CreateAccountService } from './../services/create-account.service';
import { LoginService } from '../../../shared/services/login.service';
import { FotosPessoaService } from '../services/fotos-pessoa.service';
import { MatDialog } from '@angular/material/dialog';
import { forkJoin ,switchMap } from 'rxjs';
import { DialogData } from './../../../shared/interfaces/dialog-data.interface';

@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CreateAccountComponent extends ResourceFormComponent<CreateAccountModel> {

  fotoPerfilImage = [];

  fotosPessoa = [];

  allFotosPessoa = [];

  fotoPrincipal: string;

  usuarioIdResponse : number = 0;

  constructor(
    protected createService: CreateAccountService,
    protected createUser: LoginService, 
    
    protected fotosPessoaService: FotosPessoaService,
    protected injector: Injector,
    public dialog: MatDialog) {
    super(injector, new CreateAccountModel(), createService, CreateAccountModel.fromJson, dialog);
    this.pageTitle = 'FAÇA SEU CADASTRO';
  }

  protected buildResourceForm(): void {
    this.resourceForm = this.formBuilder.group({
      personType: [false],
      nome: [null,[Validators.required, Validators.minLength(5)]],
      cpfCnpj: [null,[Validators.required, Validators.minLength(5)]],
      logradouro: [null,[Validators.required, Validators.minLength(5)]],
      numero : [null,[Validators.required, Validators.minLength(1)]],
      complemento : [null,[Validators.required, Validators.minLength(5)]],
      bairro : [null,[Validators.required, Validators.minLength(5)]],
      cep: [null,[Validators.required, Validators.minLength(5)]],
      cidade: [null,[Validators.required, Validators.minLength(5)]],
      uf: [null,[Validators.required, Validators.minLength(2)]],
      email: [null,[Validators.required, Validators.email]],
      telefone: [null,[Validators.required, Validators.minLength(5)]],
      user: [null,[Validators.required, Validators.email]],
      password: [null,[Validators.required, Validators.minLength(5)]],
      dtNascimento : [null,[Validators.required, Validators.minLength(5)]],
    });
  }

  get personTypeValue(): boolean{
    return this.getFormControls().personType.value;
  }


  protected async createResource(){

    if(!this.resourceForm.valid){
      return false;
    }


    let usuario: UserModel = {
     
      login : this.getFormControls().user.value,
      password : this.getFormControls().password.value,      
      tipo :  this.personTypeValue  ? 'J' : 'F',
    }; 
      
  

    const headers: Headers = new Headers();
    headers.set('Content-Type', 'multipart/form-data');

    let body = new FormData();

    this.allFotosPessoa.forEach(foto => {
      body.append('file', foto);
    });
    
    
    this.createUser.create(usuario, '/add').subscribe((
      data => { 
        this.usuarioIdResponse = data.id;
        let pessoa: CreateAccountModel = {

          nome: this.getFormControls().nome.value,
          cpfCnpj: this.getFormControls().cpfCnpj.value,   
          email : this.getFormControls().email.value,
          telefone : this.getFormControls().telefone.value,   
          tipo :  this.personTypeValue  ? 'J' : 'F',
        //  foto : this.fotosPessoa.toString(),
         endereco : {
          
          logradouro:  this.getFormControls().logradouro.value,
          numero : this.getFormControls().numero.value,
          cidade :  this.getFormControls().cidade.value,
          uf : this.getFormControls().uf.value,
          cep: this.getFormControls().cep.value,   
          complemento: this.getFormControls().complemento.value,
          bairro: this.getFormControls().bairro.value
    
          
        } ,    
         
          usuarioId : this.usuarioIdResponse.toString(),
          dtNascimento : this.getFormControls().dtNascimento.value,
        };
        //console.log('id =' +data.id) ;
     
    
    this.createService.create(pessoa, '/add')
      .pipe(
        switchMap(PessoaResponse => this.fotosPessoaService.create(body, `/upload/${PessoaResponse.id}`, headers))    
      )    
      .subscribe({
        next: (resource) => {

          console.log('Current Position: ', resource);
         
          this.fotosPessoaService.update('',`/profile-picture/${this.fotoPrincipal}`).subscribe();
          const dialogData: DialogData = {
            title: 'Pessoa cadastrada com sucesso!',
            closeBtn: {
              text: 'Ok',
              color: 'warn'
            }
          };
          this.matDialogConfig = {
            data: dialogData
          };
          this.actionsForSuccess()
        },
        error: (error) => {
          const dialogData: DialogData = {
            title: this.serverErrorMessages[0],
            //title: 'erro não tratado',
            closeBtn: {
              text: 'Ok',
              color: 'warn'
            }
          };
          this.matDialogConfig.data = dialogData;
          this.actionsForError()}
        
      });

    }));//user




  }

  onFileChange(evt) {
    if (evt.target.files && evt.target.files[0]) {
      var filesAmount = evt.target.files.length;
      for (let i = 0; i < filesAmount; i++) {

        var reader = new FileReader();

        if(evt.target.files[i].type === 'image/jpeg' || evt.target.files[i].type === 'image/jpg' || evt.target.files[i].type === 'image/png'){
          evt.target.id == 'fotoPerfil' ?
            (
              reader.onload = (event:any) => {
                this.fotoPrincipal = evt.target.files[i].name.split('.')[0];
                this.fotoPerfilImage = [];
                this.fotoPerfilImage.push(event.target.result);
                this.allFotosPessoa.push(evt.target.files[i]);
              },
              reader.readAsDataURL(evt.target.files[i])
            ) :
            (
              reader.onload = (event:any) => {
                if(i === 0){
                  this.fotosPessoa = [];
                }
                if(i > 4) {
                  return false;
                }
                this.fotosPessoa.push(event.target.result);
                this.allFotosPessoa.push(evt.target.files[i]);
              },
              reader.readAsDataURL(evt.target.files[i])
            );

        } else { return false; }
      }
      }
    }



}
