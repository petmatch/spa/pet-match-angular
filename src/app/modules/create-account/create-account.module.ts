import { NgxMaskModule } from 'ngx-mask';
import { SharedModule } from './../../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateAccountRoutingModule } from './create-account-routing.module';
import { CreateAccountComponent } from './components/create-account.component';



@NgModule({
  declarations: [
    CreateAccountComponent
  ],
  imports: [
    CreateAccountRoutingModule,
    SharedModule,
  ]
})
export class CreateAccountModule { }
