export class AnimalResponse{
  animalDescription: string;
  animalId: number;
  animalImage: string;
  animalName: string;
  createdAt: string;
  email: string;
  id: string;
  personId: number;
  personName: string;
  status: string;
  telephone: string;
  type: string;
  updatedAt: string;
}
