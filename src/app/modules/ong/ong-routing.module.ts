import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OngComponent } from './components/ong.component';


const routes: Routes = [
  {
    path: '',
    component: OngComponent
  },
  {
    path: 'adoptions',
    loadChildren: () =>
      import('./modules/adoptions/adoptions.module').then(m => m.AdoptionsModule)
  },
  {
    path: 'sponsorships',
    loadChildren: () =>
      import('./modules/sponsorship/sponsorship.module').then(m => m.SponsorshipModule)
  },
  {
    path: 'match-list',
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./modules/match-list/match-list.module').then(m => m.MatchListModule),
      },
      {
        path: 'detail/:id',
        loadChildren: () =>
          import('./modules/match-detail/match-detail.module').then(m => m.MatchDetailModule)
      }
    ]
  },
  {
    path: 'add-pet',
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./modules/add-pet/add-pet.module').then(m => m.AddPetModule),
      }
    ]
  },
  {
    path: 'edit-pet/:id',
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./modules/edit-pet/edit-pet.module').then(m => m.EditPetModule),
      }
    ]
  },
  {
    path: 'list-pets',
    children: [
      {
        path: '',
        loadChildren: () =>
          import('./modules/list-pets/list-pets.module').then(m => m.ListPetsModule),
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OngRoutingModule { }
