import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { OngComponent } from './components/ong.component';
import { OngRoutingModule } from './ong-routing.module';
@NgModule({
  declarations: [
    OngComponent,

  ],
  imports: [
     CommonModule,
     OngRoutingModule,
     FlexLayoutModule

  ],
  providers: [
  ],
})
export class OngModule { }
