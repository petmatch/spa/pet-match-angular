import { AnimalResponse } from './../../../../model/animal-response.model';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdoptionService } from './../../services/adoption.service';

@Component({
  selector: 'app-adoptions',
  templateUrl: './adoptions.component.html',
  styleUrls: ['./adoptions.component.scss']
})
export class AdoptionsComponent implements OnInit {

  public userId: number;

  public adoptions: AnimalResponse[];

  constructor(private route: ActivatedRoute, private adoptionService: AdoptionService) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => this.userId = params['id']);
    this.adoptionService.getOngAdoptions(this.userId).subscribe(adoptions => (
      this.adoptions = adoptions));

  }

}
