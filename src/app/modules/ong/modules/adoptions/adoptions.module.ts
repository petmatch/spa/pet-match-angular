import { AdoptionsRoutingModule } from './adoptions-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../../shared/shared.module';
import { AdoptionsComponent } from './components/adoptions/adoptions.component';
import { DetailComponent } from './components/detail/detail.component';

@NgModule({
  declarations: [
    AdoptionsComponent,
    DetailComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    AdoptionsRoutingModule
  ]
})
export class AdoptionsModule { }
