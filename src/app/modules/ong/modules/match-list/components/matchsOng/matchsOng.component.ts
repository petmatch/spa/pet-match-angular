import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatchList } from 'src/app/shared/model/match-list.interface';
import { MatchListService } from 'src/app/shared/services/match-list.service';
import { TypeEnum } from '../../enum/type.enum';

@Component({
  selector: 'app-matchsOng',
  templateUrl: './matchsOng.component.html',
  styleUrls: ['./matchsOng.component.scss']
})
export class MatchsOngComponent implements OnInit {

  public ongId: number;
  public matchs: MatchList[];
  public matchTypeEnum: TypeEnum;

  constructor(private matchListService: MatchListService, private router: ActivatedRoute) { }

  ngOnInit(): void {
    this.router.params.subscribe(params => this.ongId = params.id);
    this.matchListService.read(`/ong/${this.ongId}`).subscribe((matchs) => this.matchs = matchs);
  }

}
