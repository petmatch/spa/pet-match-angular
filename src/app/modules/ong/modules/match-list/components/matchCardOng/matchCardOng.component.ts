import { Component, Input, OnInit } from '@angular/core';
import { TypeEnum } from './../../enum/type.enum';

@Component({
  selector: 'app-matchCardOng',
  templateUrl: './matchCardOng.component.html',
  styleUrls: ['./matchCardOng.component.scss']
})
export class MatchCardOngComponent implements OnInit {
  @Input() match: {
    id: string;
    type: TypeEnum
    animalId: number;
    animalName: string;
    animalImage: string;
    personId: number;
    personName: string;
    personImage: string;
  };

  public matchTypeEnum = TypeEnum;

  constructor() { }

  ngOnInit():void {
  }

}
