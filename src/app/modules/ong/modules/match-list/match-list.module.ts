import { MatchCardOngComponent } from './components/matchCardOng/matchCardOng.component';
import { MatchsOngComponent } from './components/matchsOng/matchsOng.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatchListRoutingModule } from './match-list-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
@NgModule({
  declarations: [
    MatchsOngComponent,
    MatchCardOngComponent
  ],
  imports: [
    CommonModule,
    MatchListRoutingModule,
    FlexLayoutModule
  ],
  providers: [

  ]
})
export class MatchListModule { }
