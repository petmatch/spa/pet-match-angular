import { SponsorshipComponent } from './components/sponsorship/sponsorship.component';
import { DetailComponent } from './components/detail/detail.component';

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: SponsorshipComponent,
  },
  {
    path: ':petId/detail',
    component: DetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SponsorshipRoutingModule { }
