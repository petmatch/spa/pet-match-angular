import { SponsorshipRoutingModule } from './sponsorship-routing.module';
import { SharedModule } from './../../../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetailComponent } from './components/detail/detail.component';
import { SponsorshipComponent } from './components/sponsorship/sponsorship.component';



@NgModule({
  declarations: [
    SponsorshipComponent,
    DetailComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    SponsorshipRoutingModule
  ]
})
export class SponsorshipModule { }
