import { SponsorshipService } from './../../services/sponsorship.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sponsorship',
  templateUrl: './sponsorship.component.html',
  styleUrls: ['./sponsorship.component.scss']
})
export class SponsorshipComponent implements OnInit {

  public userId: number;

  public sponsorships: {
    id: number;
    image: string;
    name?: string;
  }[];

  constructor(private route: ActivatedRoute, private sponsorshipService: SponsorshipService) { }

  ngOnInit(): void {
    this.route.params.subscribe(params => this.userId = params['id']);
    this.sponsorshipService.getUserSponsorships(this.userId).subscribe(sponsorships => this.sponsorships = sponsorships);

  }
}
