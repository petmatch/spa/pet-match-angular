import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatchDetailRoutingModule } from './match-detail-routing.module';
import { MatchDetailComponent } from './components/match-detail.component';
import { FlexLayoutModule } from '@angular/flex-layout';
@NgModule({
  declarations: [
    MatchDetailComponent,


  ],
  imports: [
     CommonModule,
     MatchDetailRoutingModule,
     FlexLayoutModule

  ],
  providers: [

  ]
})
export class MatchDetailModule { }
