import { Component, OnInit } from '@angular/core';
import { MatchListService } from '../../../../../shared/services/match-list.service';
import { MatchList } from '../../../../../shared/model/match-list.interface';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-match-detail',
  templateUrl: './match-detail.component.html',
  styleUrls: ['./match-detail.component.scss']
})



export class MatchDetailComponent implements OnInit {

  public matchList: MatchList[];
  public matchListById: MatchList[];
  private sub: any;
  public id : number;


  constructor(private route: ActivatedRoute,private matchListService: MatchListService) { }



  async ngOnInit() {

    this.sub = await  this.route.params.subscribe(params => {
      this.id = +params['id'];
   });

   await this.matchListService.read().subscribe((matchList) =>  this.matchList = matchList.filter(match => match.id === this.id));

  }

}
