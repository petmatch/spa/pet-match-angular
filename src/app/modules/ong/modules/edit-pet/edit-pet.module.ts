import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../shared/shared.module';
import { EditPetRoutingModule } from './edit-pet-routing.module';
import { EditPetComponent } from './components/edit-pet.component';

@NgModule({
  declarations: [
    EditPetComponent
  ],
  imports: [
    SharedModule,
    EditPetRoutingModule
  ]
})
export class EditPetModule { }
