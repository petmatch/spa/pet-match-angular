import { PetResponseModel } from './../model/pet-response.model';
import { MatDialog } from '@angular/material/dialog';
import { DialogData } from '../../../../../shared/interfaces/dialog-data.interface';
import { Component, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { switchMap } from 'rxjs';
import { ResourceFormComponent } from './../../../../../shared/components/resource-form/resource-form.component';
import { FotosPetService } from '../../../services/fotos-pet.service';
import { DisponibilidadeEnum } from './../../add-pet/enums/disponibilidade.enum';
import { PetModel } from './../../add-pet/model/pet.model';
import { AddPetService } from '../../../services/add-pet.service';

@Component({
  selector: 'app-add-pet',
  templateUrl: './edit-pet.component.html',
  styleUrls: ['./edit-pet.component.scss'],
})
export class EditPetComponent extends ResourceFormComponent<PetModel>{

  fotoPerfilImage = [];

  fotosPet = [];

  allFotosPet = [];

  fotoPrincipal: string;

  public petId: number;
  public ongID: number;

  public pet: PetResponseModel;

  constructor(
    protected addPetService: AddPetService,
    protected fotosPetService: FotosPetService,
    protected injector: Injector,
    public dialog: MatDialog) {
    super(injector, new PetModel(), addPetService, PetModel.fromJson, dialog);

    this.pageTitle = 'CADASTRO DE ANIMAL';
    this.route.params.subscribe((params) => {
      this.ongID = params['ongId'];
    });

    this.loadResource();
    console.log(this.resource);
  }

  protected buildResourceForm(): void {

    console.log(this.pet);

    this.resourceForm = this.formBuilder.group({
      petType: [this.pet.tipoAnimal.domestico],
      nome: [this.pet.nome,[Validators.required, Validators.minLength(5)]],
      especie: [this.pet.tipoAnimal.especie,[Validators.required, Validators.minLength(5)]],
      raca: [this.pet.tipoAnimal.raca,[Validators.required, Validators.minLength(3)]],
      sexo: [this.pet.tipoAnimal.sexo,[Validators.required, Validators.minLength(5)]],
      dtNascimento: [this.pet.dtNascimento,[Validators.required, Validators.minLength(5)]],
      dtResgate: [this.pet.dtResgate,[Validators.required, Validators.minLength(5)]],
      adocao: [this.pet.disponibilidade[0]],
      apadrinhamento: [this.pet.disponibilidade[1]],
      cartaApresentacao: [this.pet.cartaApresentacao],
    });
  }

  get petTypeValue(): boolean{
    return this.getFormControls().petType.value;
  }

  protected createResource(){

    if(!this.resourceForm.valid){
      return false;
    }
    this.isLoading = true;

    let adocao: string = this.getFormControls().adocao.value ? 'Adoção' : '';
    let apadrinhamento: string =  this.getFormControls().apadrinhamento.value ? 'Apadrinhamento' : '';

    let disponibilidade: DisponibilidadeEnum[] =
      [
        adocao === '' ? null : DisponibilidadeEnum[adocao],
        apadrinhamento === '' ? null : DisponibilidadeEnum[apadrinhamento]
      ];

    let pet: PetModel = {
      adotado: this.getFormControls().adocao.value,
      cartaApresentacao: this.getFormControls().cartaApresentacao.value,
      disponibilidade: disponibilidade,
      dtNascimento: this.getFormControls().dtNascimento.value,
      dtResgate: this.getFormControls().dtResgate.value,
      ong: this.ongID,
      nome: this.getFormControls().nome.value,
      tipoAnimal: {
        domestico: !this.getFormControls().petType.value,
        especie: this.getFormControls().especie.value,
        raca: this.getFormControls().raca.value,
        sexo: this.getFormControls().sexo.value
      },
      qtdMatch: 0

    };

    const headers: Headers = new Headers();
    headers.set('Content-Type', 'multipart/form-data');

    let body = new FormData();

    this.allFotosPet.forEach(foto => {
      body.append('file', foto);
    });

    this.addPetService.create(pet, '/add')
      .pipe(
        switchMap(petResponse => this.fotosPetService.create(body, `/upload/${petResponse.id}`, headers))
      )
      .subscribe({
        next: (resource) => {
          this.fotosPetService.update('',`/profile-picture/${this.fotoPrincipal}`).subscribe();
          const dialogData: DialogData = {
            title: 'Animal cadastrado com sucesso!',
            closeBtn: {
              text: 'Ok',
              color: 'warn'
            }
          };
          this.matDialogConfig = {
            data: dialogData
          };
          this.redirectPath = `/ong/${this.ongID}/list-pets`;
          this.isLoading = false;
          this.actionsForSuccess();
        },
        error: (error) => {
          const dialogData: DialogData = {
            title: this.serverErrorMessages[0],
            closeBtn: {
              text: 'Ok',
              color: 'warn'
            }
          };
          this.matDialogConfig.data = dialogData;
          this.isLoading = false;
          this.actionsForError()}
      });




  }

  onFileChange(evt) {
    if (evt.target.files && evt.target.files[0]) {
      var filesAmount = evt.target.files.length;
      for (let i = 0; i < filesAmount; i++) {

        var reader = new FileReader();

        if(evt.target.files[i].type === 'image/jpeg' || evt.target.files[i].type === 'image/jpg' || evt.target.files[i].type === 'image/png'){
          evt.target.id == 'fotoPerfil' ?
            (
              reader.onload = (event:any) => {
                this.fotoPrincipal = evt.target.files[i].name.split('.')[0].replace(' ', '-');
                this.fotoPerfilImage = [];
                this.fotoPerfilImage.push(event.target.result);
                this.allFotosPet.push(evt.target.files[i]);
              },
              reader.readAsDataURL(evt.target.files[i])
            ) :
            (
              reader.onload = (event:any) => {
                if(i === 0){
                  this.fotosPet = [];
                }
                if(i > 4) {
                  return false;
                }
                this.fotosPet.push(event.target.result);
                this.allFotosPet.push(evt.target.files[i]);
              },
              reader.readAsDataURL(evt.target.files[i])
            );

        } else { return false; }
      }
    }
  }
}
