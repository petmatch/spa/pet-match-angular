import { TipoAnimalModel } from './../../add-pet/model/tipo-animal.model';
import { DisponibilidadeEnum } from './../../add-pet/enums/disponibilidade.enum';
import { ResourceModel } from './../../../../../shared/model/resource.model';

export class PetResponseModel extends ResourceModel{
  constructor(
    public ong?: number,
    public nome?: string,
    public dtNascimento?: string,
    public dtResgate?: string,
    public disponibilidade?: DisponibilidadeEnum[],
    public cartaApresentacao?: string,
    public fotos?: FotosPetModel[],
    public qtdMatch: number = 0,
    public adotado: boolean = false,
    public tipoAnimal?: TipoAnimalModel,
  ){
    super();
  }
  static fromJson(jsonData: any): PetResponseModel {
    return Object.assign(new PetResponseModel(), jsonData);
  }
}

class FotosPetModel{
  public id?: number;
  public isprofilePicture?: boolean;
}
