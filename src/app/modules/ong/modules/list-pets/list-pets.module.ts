import { FormsModule } from '@angular/forms';
import { FilterPipe } from './pipe/filter.pipe';
import { SharedModule } from './../../../../shared/shared.module';
import { ListPetsRoutingModule } from './list-pets-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListPetsComponent } from './components/list-pets.component';



@NgModule({
  declarations: [
    ListPetsComponent,
    FilterPipe
  ],
  imports: [
    CommonModule,
    SharedModule,
    ListPetsRoutingModule,
    FormsModule,
  ]
})
export class ListPetsModule { }
