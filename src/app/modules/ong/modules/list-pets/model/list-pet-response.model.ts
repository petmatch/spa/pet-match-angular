import { TipoAnimalModel } from './../../add-pet/model/tipo-animal.model';
import { DisponibilidadeEnum } from './../../add-pet/enums/disponibilidade.enum';
import { ResourceModel } from './../../../../../shared/model/resource.model';
export class ListPetResponse extends ResourceModel{
  constructor(
  public id?: number,
  public ong?: number,
  public nome?: string,
  public dtNascimento?: string,
  public foto?: string,
  public dtResgate?: string,
  public disponibilidade?: DisponibilidadeEnum[],
  public cartaApresentacao?: string,
  public qtdMatch?: number,
  public adotado?: boolean,
  public tipoAnimal?: TipoAnimalModel,) {
    super();
  }

  static fromJson(jsonData: any): ListPetResponse {
    return Object.assign(new ListPetResponse(), jsonData);
  }
}
