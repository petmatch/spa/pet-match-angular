import { ListPetResponse } from './../model/list-pet-response.model';
import { ListPetModel } from './../model/list-pet.model';
import { ActivatedRoute } from '@angular/router';
import { OngPetService } from './../../../services/ong-pet.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-pets',
  templateUrl: './list-pets.component.html',
  styleUrls: ['./list-pets.component.scss']
})
export class ListPetsComponent implements OnInit {

  public ongID: number;
  public pageTitle: string;
  public listPets: ListPetModel[] = [];
  public petSearch: string;


  constructor(
    protected ongPetService: OngPetService,
    private route: ActivatedRoute
    ) {

    this.route.params.subscribe((params) => this.ongID = params['id']);

    this.pageTitle = 'ANIMAIS CADASTRADOS';
  }

  ngOnInit(): void {
    this.getPets();
  }

  getPets(){

    this.ongPetService.read(`/${this.ongID}/animais/profile-picture`).subscribe(
      (animais) => {
        console.log(animais);
        animais.forEach((animal) => {
          const animalRes: ListPetModel = {
            id: animal.id,
            nome: animal.nome,
            foto: animal.foto,
            dtRegistro: animal.dtResgate
          };
          this.listPets.push(animalRes);
        });
      });
  }

}
