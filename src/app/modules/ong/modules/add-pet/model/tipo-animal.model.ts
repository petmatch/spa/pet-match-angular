export class TipoAnimalModel{
  public especie: string;
  public sexo: string;
  public raca: string;
  public cor?: string;
  public domestico: boolean;
}
