import { TipoAnimalModel } from './tipo-animal.model';
import { DisponibilidadeEnum } from './../enums/disponibilidade.enum';
import { ResourceModel } from './../../../../../shared/model/resource.model';

export class PetModel extends ResourceModel{
  constructor(
    public ong?: number,
    public nome?: string,
    public dtNascimento?: string,
    public dtResgate?: string,
    public disponibilidade?: DisponibilidadeEnum[],
    public cartaApresentacao?: string,
    public qtdMatch: number = 0,
    public adotado: boolean = false,
    public tipoAnimal?: TipoAnimalModel,
  ){
    super();
  }

  static fromJson(jsonData: any): PetModel {
    return Object.assign(new PetModel(), jsonData);
  }
}
