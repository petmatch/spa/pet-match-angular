import { ResourceModel } from './../../../../../shared/model/resource.model';

export class FotoPetModel extends ResourceModel{

  constructor(
    public file?: File,
  ){
    super();
  }


  static fromJson(jsonData: any): FotoPetModel[] {
    return Object.assign(new FotoPetModel(), jsonData);
  }
}
