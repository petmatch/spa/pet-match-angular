import { NgModule } from '@angular/core';
import { SharedModule } from './../../../../shared/shared.module';
import { AddPetRoutingModule } from './add-pet-routing.module';
import { AddPetComponent } from './components/add-pet.component';

@NgModule({
  declarations: [
    AddPetComponent
  ],
  imports: [
    SharedModule,
    AddPetRoutingModule
  ]
})
export class AddPetModule { }
