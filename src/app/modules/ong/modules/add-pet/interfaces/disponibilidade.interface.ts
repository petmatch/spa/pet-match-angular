import { DisponibilidadeEnum } from './../enums/disponibilidade.enum';
export interface IDisponibilidade {
  disponibilidade: DisponibilidadeEnum;
  isActive: boolean;
}
