import { MatDialog } from '@angular/material/dialog';
import { DialogData } from './../../../../../shared/interfaces/dialog-data.interface';
import { Component, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { switchMap } from 'rxjs';
import { ResourceFormComponent } from 'src/app/shared/components/resource-form/resource-form.component';
import { FotosPetService } from '../../../services/fotos-pet.service';
import { DisponibilidadeEnum } from './../enums/disponibilidade.enum';
import { PetModel } from './../model/pet.model';
import { AddPetService } from '../../../services/add-pet.service';

@Component({
  selector: 'app-add-pet',
  templateUrl: './add-pet.component.html',
  styleUrls: ['./add-pet.component.scss'],
})
export class AddPetComponent extends ResourceFormComponent<PetModel>{

  fotoPerfilImage = [];

  fotosPet = [];

  allFotosPet = [];

  fotoPrincipal: string;

  public ongID: number;

  constructor(
    protected addPetService: AddPetService,
    protected fotosPetService: FotosPetService,
    protected injector: Injector,
    public dialog: MatDialog) {
    super(injector, new PetModel(), addPetService, PetModel.fromJson, dialog);

    this.pageTitle = 'CADASTRAR ANIMAL';
    this.route.params.subscribe((params) => this.ongID = params['ongId']);
   }
   protected buildResourceForm(): void {
    this.resourceForm = this.formBuilder.group({
      petType: [false],
      nome: [null,[Validators.required, Validators.minLength(5)]],
      especie: [null,[Validators.required, Validators.minLength(5)]],
      raca: [null,[Validators.required, Validators.minLength(3)]],
      sexo: [null,[Validators.required, Validators.minLength(5)]],
      dtNascimento: [null,[Validators.required, Validators.minLength(5)]],
      dtResgate: [null,[Validators.required, Validators.minLength(5)]],
      adocao: [false],
      apadrinhamento: [false],
      cartaApresentacao: [null],
    });
  }

  get petTypeValue(): boolean{
    return this.getFormControls().petType.value;
  }

  protected createResource(){

    if(!this.resourceForm.valid){
      return false;
    }
    this.isLoading = true;

    let adocao: string = this.getFormControls().adocao.value ? 'Adoção' : '';
    let apadrinhamento: string =  this.getFormControls().apadrinhamento.value ? 'Apadrinhamento' : '';

    let disponibilidade: DisponibilidadeEnum[] =
      [
        adocao === '' ? null : DisponibilidadeEnum[adocao],
        apadrinhamento === '' ? null : DisponibilidadeEnum[apadrinhamento]
      ];

    let pet: PetModel = {
      adotado: false,
      cartaApresentacao: this.getFormControls().cartaApresentacao.value,
      disponibilidade: disponibilidade,
      dtNascimento: this.getFormControls().dtNascimento.value,
      dtResgate: this.getFormControls().dtResgate.value,
      ong: this.ongID,
      nome: this.getFormControls().nome.value,
      tipoAnimal: {
        domestico: !this.getFormControls().petType.value,
        especie: this.getFormControls().especie.value,
        raca: this.getFormControls().raca.value,
        sexo: this.getFormControls().sexo.value
      },
      qtdMatch: 0

    };

    const headers: Headers = new Headers();
    headers.set('Content-Type', 'multipart/form-data');

    let body = new FormData();

    this.allFotosPet.forEach(foto => {
      body.append('file', foto);
    });

    this.addPetService.create(pet, '/add')
      .pipe(
        switchMap(petResponse => this.fotosPetService.create(body, `/upload/${petResponse.id}`, headers))
      )
      .subscribe({
        next: (resource) => {
          this.fotosPetService.update('',`/profile-picture/${this.fotoPrincipal}`).subscribe();
          const dialogData: DialogData = {
            title: 'Animal cadastrado com sucesso!',
            closeBtn: {
              text: 'Ok',
              color: 'warn'
            }
          };
          this.matDialogConfig = {
            data: dialogData
          };
          this.redirectPath = `/ong/${this.ongID}/list-pets`;
          this.isLoading = false;
          this.actionsForSuccess();
        },
        error: (error) => {
          const dialogData: DialogData = {
            title: this.serverErrorMessages[0],
            closeBtn: {
              text: 'Ok',
              color: 'warn'
            }
          };
          this.matDialogConfig.data = dialogData;
          this.isLoading = false;
          this.actionsForError()}
      });




  }

  onFileChange(evt) {
    if (evt.target.files && evt.target.files[0]) {
      var filesAmount = evt.target.files.length;
      for (let i = 0; i < filesAmount; i++) {

        var reader = new FileReader();

        if(evt.target.files[i].type === 'image/jpeg' || evt.target.files[i].type === 'image/jpg' || evt.target.files[i].type === 'image/png'){
          evt.target.id == 'fotoPerfil' ?
            (
              reader.onload = (event:any) => {
                this.fotoPrincipal = evt.target.files[i].name.split('.')[0].replace(' ', '-');
                this.fotoPerfilImage = [];
                this.fotoPerfilImage.push(event.target.result);
                this.allFotosPet.push(evt.target.files[i]);
              },
              reader.readAsDataURL(evt.target.files[i])
            ) :
            (
              reader.onload = (event:any) => {
                if(i === 0){
                  this.fotosPet = [];
                }
                if(i > 4) {
                  return false;
                }
                this.fotosPet.push(event.target.result);
                this.allFotosPet.push(evt.target.files[i]);
              },
              reader.readAsDataURL(evt.target.files[i])
            );

        } else { return false; }
      }
    }
  }
}
