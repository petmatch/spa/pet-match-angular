import { urlsConfig } from './../../../config/urls.config';
import { ListPetResponse } from './../modules/list-pets/model/list-pet-response.model';
import { CrudService } from './../../../shared/services/crud.service';
import { Injectable, Injector } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OngPetService extends CrudService<ListPetResponse> {

  constructor(protected injector: Injector) {
    super(
      urlsConfig.ongPets,
      injector,
      ListPetResponse.fromJson
    )
  }
}
