import { Injectable, Injector } from '@angular/core';
import { CrudService } from '../../../shared/services/crud.service';
import { urlsConfig } from '../../../config/urls.config';
import { FotoPetModel } from '../modules/add-pet/model/foto-pet.model';

@Injectable({
  providedIn: 'root'
})
export class FotosPetService extends CrudService<any> {

  constructor(protected injector: Injector) {
    super(
      urlsConfig.fotosPet,
      injector,
      FotoPetModel.fromJson
    )
  }

}
