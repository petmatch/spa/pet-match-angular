import { TestBed } from '@angular/core/testing';

import { FotosPetService } from './fotos-pet.service';

describe('FotosPetService', () => {
  let service: FotosPetService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FotosPetService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
