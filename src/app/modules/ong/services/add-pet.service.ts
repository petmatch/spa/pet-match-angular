import { urlsConfig } from '../../../config/urls.config';
import { PetModel } from '../modules/add-pet/model/pet.model';
import { CrudService } from 'src/app/shared/services/crud.service';
import { Inject, Injectable, Injector } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AddPetService extends CrudService<PetModel> {

  constructor(protected injector: Injector) {
    super(
      urlsConfig.addPet,
      injector,
      PetModel.fromJson
    )
  }
}
