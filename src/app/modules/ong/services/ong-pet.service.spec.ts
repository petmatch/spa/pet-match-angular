import { TestBed } from '@angular/core/testing';

import { OngPetService } from './ong-pet.service';

describe('OngPetService', () => {
  let service: OngPetService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OngPetService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
