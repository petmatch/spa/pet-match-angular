import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserComponent } from './components/user/user.component';
import { MatchsComponent } from './modules/matchs/components/matchs/matchs.component';
import { AdoptionsComponent } from './modules/adoptions/components/adoptions/adoptions.component';

const routes: Routes = [
  {
    path: '',
    component: UserComponent
  },
  {
    path: 'matchs',
    component: MatchsComponent
  },
  {
    path: 'adoptions',
    loadChildren: () =>
      import('./modules/adoptions/adoptions.module').then(m => m.AdoptionsModule)
  },
  {
    path: 'sponsorships',
    loadChildren: () =>
      import('./modules/sponsorship/sponsorship.module').then(m => m.SponsorshipModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
