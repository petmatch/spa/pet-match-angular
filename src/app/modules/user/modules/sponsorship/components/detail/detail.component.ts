import { ListPetResponse } from './../../../../../ong/modules/list-pets/model/list-pet-response.model';
import { DetailService } from './../../services/detail.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

  public petId: string;

  public animalRes: ListPetResponse = new ListPetResponse();

  constructor(private route: ActivatedRoute, private detailService: DetailService) {

    this.route.params.subscribe(params => this.petId = params['petId']);
  }

  ngOnInit(): void {
    this.detailService.getAnimalDetail(this.petId).subscribe(animal => this.animalRes = animal);
  }

}
