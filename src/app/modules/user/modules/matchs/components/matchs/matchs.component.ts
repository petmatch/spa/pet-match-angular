import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatchService } from '../../service/match.service';

@Component({
  selector: 'app-matchs',
  templateUrl: './matchs.component.html',
  styleUrls: ['./matchs.component.scss'],
})
export class MatchsComponent implements OnInit {
  public matchs: {
    id: number;
    image: string;
    name: string;
    type: string;
    status: string;
  }[];

  public userId: number;

  constructor(
    private route: ActivatedRoute, private matchService: MatchService) {}

  ngOnInit(): void {
    this.route.params.subscribe(params => this.userId = params['id']);
    this.matchService.getUserMatchs(this.userId).subscribe(matchs => this.matchs = matchs);
  }
}
