import { Component, Input, OnInit } from '@angular/core';
import { StatusEnum } from './../../enums/status.enum';
import { TypeEnum } from 'src/app/modules/ong/modules/match-list/enum/type.enum';

@Component({
  selector: 'app-matchCard',
  templateUrl: './matchCard.component.html',
  styleUrls: ['./matchCard.component.scss'],
})
export class MatchCardComponent implements OnInit {
  @Input() match: {
    id: number;
    image: string;
    name: string;
    type: string;
    status: string;
  };

  public matchTypeEnum = TypeEnum;
  public matchStatusEnum = StatusEnum;

  public options: StatusEnum[] = [StatusEnum.WAITING, StatusEnum.IN_ANALYSIS, StatusEnum.APPROVED];

  constructor() {}

  ngOnInit(): void {
    console.log(this.matchStatusEnum.APPROVED)
  }
}
