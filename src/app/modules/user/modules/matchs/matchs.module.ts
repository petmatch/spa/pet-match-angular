import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatchsComponent } from './components/matchs/matchs.component';
import { MatchCardComponent } from './components/matchCard/matchCard.component';

@NgModule({
  declarations: [
    MatchsComponent,
    MatchCardComponent
  ],
  imports: [
    CommonModule
  ]
})
export class MatchsModule { }
