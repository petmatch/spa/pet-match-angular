import { ListPetResponse } from './../../../../ong/modules/list-pets/model/list-pet-response.model';
import { map } from 'rxjs/operators';
import { urlsConfig } from './../../../../../config/urls.config';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DetailService {

  constructor(protected httpClient: HttpClient) { }

  public getAnimalDetail(id: string): Observable<ListPetResponse> {
    return this.httpClient.get(`${urlsConfig.addPet}/${id}/profile`).pipe(
      map((item: ListPetResponse) => {
        return item;
      })
    );
  }
}
