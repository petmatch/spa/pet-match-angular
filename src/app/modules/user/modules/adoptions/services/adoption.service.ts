import { urlsConfig } from '../../../../../config/urls.config';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AdoptionService {

  constructor(protected httpClient: HttpClient) { }

  public getUserAdoptions(id: number): Observable<any> {
    return this.httpClient.get(`${urlsConfig.users}/user/${id}/adoptions`).pipe(
      map((item: any) => {
        return item;
      })
    );
  }

}
