import { AdoptionsComponent } from './components/adoptions/adoptions.component';
import { DetailComponent } from './components/detail/detail.component';

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: AdoptionsComponent,
  },
  {
    path: ':petId/detail',
    component: DetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdoptionsRoutingModule { }
