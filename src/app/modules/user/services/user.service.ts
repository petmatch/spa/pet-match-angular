import { urlsConfig } from '../../../config/urls.config';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(protected httpClient: HttpClient) {}

  public getUser(id: number): Observable<any> {
    return this.httpClient.get(`${urlsConfig.users}/${id}`).pipe(
      map((item: any) => {
        return item;
      })
    );
  }
}
