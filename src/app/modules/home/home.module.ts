import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { SwiperModule } from 'swiper/angular';
import { SharedModule } from './../../shared/shared.module';
import { AdoptionsComponent } from './components/adoptions/adoptions.component';
import { DisclaimerComponent } from './components/disclaimer/disclaimer.component';
import { HomeComponent } from './components/home/home.component';
import { SmallGalleryComponent } from './components/small-gallery/small-gallery.component';
import { SwiperInternComponent } from './components/swiper/swiper-intern.component';
import { HomeRoutingModule } from './home-routing.module';
import { PetService } from './services/pet.service';
import { MatchDialogComponent } from './components/match-dialog/match-dialog.component';



@NgModule({
  declarations: [
    HomeComponent,
    SwiperInternComponent,
    DisclaimerComponent,
    AdoptionsComponent,
    SmallGalleryComponent,
    MatchDialogComponent
  ],
  imports: [
    HttpClientModule,
    HomeRoutingModule,
    SharedModule,
    SwiperModule
  ],
  providers: [
    PetService
  ]
})
export class HomeModule { }
