import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatchRequestModel } from './../../model/match-request.model';
import { HomeMatchService } from './../../services/home-match.service';

@Component({
  selector: 'app-match-dialog',
  templateUrl: './match-dialog.component.html',
  styleUrls: ['./match-dialog.component.scss']
})
export class MatchDialogComponent implements OnInit {

  public pageTitle = 'Escolha seu tipo de match';
  public isMatched: boolean = false;

  constructor(
    public dialogRef: MatDialogRef<MatchDialogComponent>,
    protected matDialog: MatDialog,
    private homeMatchService: HomeMatchService) { }

  ngOnInit(): void {
  }

  close(): void {
    this.dialogRef.close();
  }

  doMatch(type: string){
    const matchRequest: MatchRequestModel = new MatchRequestModel();
    matchRequest.type = type;
    this.homeMatchService.createMatch(matchRequest).subscribe(match => {
      this.isMatched = true;
    });
  }
}
