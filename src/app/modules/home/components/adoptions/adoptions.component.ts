import { ITitleDescription } from './../../../../shared/interfaces/title-description.interface';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-adoptions',
  templateUrl: './adoptions.component.html',
  styleUrls: ['./adoptions.component.scss']
})
export class AdoptionsComponent implements OnInit {

  @Input() adoption: ITitleDescription;

  constructor() { }

  ngOnInit(): void {
  }

}
