import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-disclaimer',
  templateUrl: './disclaimer.component.html',
  styleUrls: ['./disclaimer.component.scss']
})
export class DisclaimerComponent {
  @Input() personText: string;
  @Input() linkText: string;

  constructor() { }

}
