import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import obj from './../../mocks/infos-home.json';
import { HomeModel } from './../../model/home.model';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class HomeComponent implements OnInit {

  public homeInfos: HomeModel;
  public typePerson: string;
  public personText: string;
  public linkText: string;
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,) {
      this.router.routeReuseStrategy.shouldReuseRoute = () => false;

    }

  ngOnInit(): void {
    this.typePerson = this.activatedRoute.snapshot.paramMap.get('typeOfPerson');
    this.addText(this.typePerson);
    this.homeInfos = obj;
  }

  addText(person: string){
    switch(person){
      case 'PF':
        this.personText = 'VER PETS';
        this.linkText = '/user';
        break;
      case 'PJ':
        this.personText = 'ADMNISTRAR';
        this.linkText = '/ong';
        break;
      default:
        this.personText = 'FAÇA SEU CADASTRO';
        this.linkText = '/create-account';
        break;

    }
  }

}
