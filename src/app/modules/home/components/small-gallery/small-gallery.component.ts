import { HomeImagesGalery } from './../../model/home.model';
import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { SwiperComponent } from "swiper/angular";


@Component({
  selector: 'app-small-gallery',
  templateUrl: './small-gallery.component.html',
  styleUrls: ['./small-gallery.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class SmallGalleryComponent implements OnInit {

  @Input() gallery:HomeImagesGalery;

  constructor() { }

  ngOnInit(): void {
  }

}
