import { MatchDialogComponent } from './../match-dialog/match-dialog.component';
import { last, Observable } from 'rxjs';
import { PetModel } from './../../model/pet.model';
import { PetService } from './../../services/pet.service';
import { AfterViewInit, Component, ElementRef, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
// import Swiper core and required modules
import SwiperCore, { Manipulation, Navigation, Swiper } from 'swiper';
import { DialogComponent } from './../../../../shared/components/dialog/dialog.component';
import { DialogData } from './../../../../shared/interfaces/dialog-data.interface';


// install Swiper modules
SwiperCore.use([Navigation, Manipulation ]);

@Component({
  selector: 'app-swiper',
  templateUrl: './swiper-intern.component.html',
  styleUrls: ['./swiper-intern.component.scss']
})
export class SwiperInternComponent implements OnInit, AfterViewInit {

  public swiper: Swiper;
  public pet: Observable<PetModel>;

  constructor(
    private elementRef: ElementRef,
    public dialog: MatDialog,
    private petService: PetService) {
   }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.constructSwiper();
  }

  deleteSlide(){
    const lastSlide: boolean = this.swiper.slides.length - 1 === 0;
    if(lastSlide){
      const matDialogData: DialogData = {
        title: 'Todos os animais disponíveis já foram visualizados. Deseja reiniciar ?',
        closeBtn: {
          text: 'Fechar',
          color: 'primary'
        },
        saveBtn: {
          text: 'Sim',
          callback: () => {
            this.swiper.removeAllSlides();
            this.swiper.enable();
            this.constructSwiper();
            this.dialog.closeAll();
          }
        }
      }
      const matDialogConfig: MatDialogConfig = {
        panelClass: 'dialogClass',
        data: matDialogData,
        height: '30vh'
      }
      this.dialog.open(DialogComponent, matDialogConfig );
      this.swiper.disable();
    } else {
      this.swiper.removeSlide(this.swiper.realIndex);
      this.swiper.update();
    }
  }

  constructSwiper(){
    this.swiper = new Swiper(this.elementRef.nativeElement.querySelector('.swiper'), {
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
      loop: false,
      pagination: false,
      cssMode: true,
      slidesPerView: 'auto',
      direction: 'horizontal',
    });
    this.addSlides();
  }

  addSlides(){
    this.swiper.appendSlide([
      `<div class="swiper-slide">
        <a (click)="${this.seePet(0)}">
          <img src="./../../../../../assets/animals/pug.png" alt="" srcset="">
        </a>
      </div>`,
      '<div class="swiper-slide"><img src="./../../../../../assets/animals/vira-lata.png" alt="" srcset=""></div>',
      '<div class="swiper-slide"><img src="./../../../../../assets/animals/pug.png" alt="" srcset=""></div>',
      '<div class="swiper-slide"><img src="./../../../../../assets/animals/vira-lata.png" alt="" srcset=""></div>'
     ]);
    this.swiper.update();
  }

  seePet(petId: number){
    this.pet = this.petService.getById(petId);
  }

  openDialog() {
      this.dialog.open(MatchDialogComponent, {autoFocus: false});
  }
}
