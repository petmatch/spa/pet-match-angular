import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SwiperInternComponent } from './swiper-intern.component';

describe('SwiperInternComponent', () => {
  let component: SwiperInternComponent;
  let fixture: ComponentFixture<SwiperInternComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SwiperInternComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SwiperInternComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
