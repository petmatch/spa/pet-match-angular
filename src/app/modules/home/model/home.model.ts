import { ITitleDescription } from './../../../shared/interfaces/title-description.interface';
export class HomeModel {
  constructor(
    public ongs?: ITitleDescription,
    public ultimasAdocoes?: ITitleDescription[],
    public galerias?: HomeImagesGalery[],
    public eventos?: ITitleDescription
  ){

  }

  static fromJson(jsonData: any): HomeModel {
    return Object.assign(new HomeModel(), jsonData);
  }
}

export class HomeImagesGalery{
  public title: string;
  public imagens: string[];
}

