import { ResourceModel } from "./../../../shared/model/resource.model";

export class MatchResponseModel extends ResourceModel{

  constructor(
    animalID?: number,
    createdAt?: string,
    ongID?: number,
    personID?: number,
    status?: string,
    type?: string,
    updatedAt?: string,
  ){
    super();
  }


  static fromJson(jsonData: any): MatchResponseModel {
    return Object.assign(new MatchResponseModel(), jsonData);
  }
}
