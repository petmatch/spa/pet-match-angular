import { ResourceModel } from "./../../../shared/model/resource.model"

export class PetModel extends ResourceModel{

  constructor(
    public pet?: string,
    public sexo?: string,
    public raca?: string,
    public idade?: number,
    public ong?: string,
    public dataAcolhimento?: string,
    public foto?: string,
    public description?: string
    ){
    super();
  }

  static fromJson(jsonData: any): PetModel {
    return Object.assign(new PetModel(), jsonData);
  }
}
