export class MatchRequestModel {
  personID: number;
  ongID: number;
  animalID: number;
  type: string;
}
