import { TestBed } from '@angular/core/testing';

import { HomeMatchService } from './home-match.service';

describe('HomeMatchService', () => {
  let service: HomeMatchService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HomeMatchService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
