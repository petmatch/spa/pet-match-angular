import { PetModel } from './../model/pet.model';
import { urlsConfig } from './../../../config/urls.config';
import { CrudService } from 'src/app/shared/services/crud.service';
import { Injectable, Injector } from '@angular/core';

@Injectable()
export class PetService extends CrudService<PetModel> {

  constructor(
    protected injector: Injector,
  ) {
    super(
      urlsConfig.pets,
      injector,
      PetModel.fromJson
    );
  }
}
