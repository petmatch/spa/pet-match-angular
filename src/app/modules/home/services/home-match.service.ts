import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { MatchRequestModel } from './../model/match-request.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { urlsConfig } from './../../../config/urls.config';
import { MatchResponseModel } from './../model/match-response.model';

@Injectable({
  providedIn: 'root'
})
export class HomeMatchService {

  constructor(private http: HttpClient
  ) {}

  createMatch(request: MatchRequestModel): Observable<MatchResponseModel>{
    return this.http.post(urlsConfig.match, request).pipe(
      map(match => match)
    )
  }
}
